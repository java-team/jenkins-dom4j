#!/bin/sh -e

VERSION=$2
TAR=../jenkins-dom4j_$VERSION.orig.tar.xz
DIR=jenkins-dom4j-$VERSION
mkdir -p $DIR

# Expand the upstream tarball
tar -xzf $3 -C $DIR --strip-components=1
rm $3

# Repack excluding stuff we don't need
XZ_OPT=--best tar -v -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude 'CVS' \
    --exclude '.svn' \
    --exclude 'src/java/org/dom4j/tree/ConcurrentReaderHashMap.java' \
    $DIR
rm -rf $DIR

